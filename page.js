class Tache {
    constructor(titre, id, list) {
        this.list = list;
        this.titre = titre;
        this.estTermine = false;
        this.id = id;
        this.createdHtmlElement = document.createElement('li');

        this.createdHtmlElement.setAttribute('class', "todo list-group-item d-flex align-items-center");
        this.createdHtmlElement.innerHTML = `
        <input class="form-check-input" type="checkbox" id="todo-${id}">
        <label class="ms-2 form-check-label" for="todo-${id}">
            ${this.titre}
        </label>
        <label class="ms-auto btn btn-danger btn-sm">
        <i class="bi-trash">
        </i>
        </label>`;
        this.createdHtmlElement.querySelector(`#todo-${id}`).addEventListener('click', () => this.estTermine = document.querySelector(`#todo-${id}`).checked);
        this.createdHtmlElement.querySelector('.bi-trash').addEventListener('click', () => { this.list.removeTask(this); reloadListOnHTML(); });
    }
}

class Liste {
    constructor() {
        this.taches = [];
        this.createdHtmlElement = document.querySelector('body > section > main > ul');
        this.listFiltered = this.getAllTasks
    }

    ajouterTache(tache) {
        this.taches.push(tache);
    }

    terminerTache(tache) {
        tache.terminer();
    }

    removeTask(tache) {
        this.taches = this.taches.filter(tacheInList => tacheInList !== tache);
    }

    getAllTasks() {
        return this.taches;
    }

    getTasksWhereEstTermine() {
        this.listFiltered = this.getTasksWhereEstTermine;
        return this.taches.filter(tache => tache.estTermine);
    }

    getTasksWhereNotEstTermine() {
        this.listFiltered = this.getTasksWhereNotEstTermine;
        return this.taches.filter(tache => !tache.estTermine);
    }

    setAllTasks() {
        this.listFiltered = this.getAllTasks;
    }

    setTasksWhereEstTermine() {
        this.listFiltered = this.getTasksWhereEstTermine;
    }

    setTasksWhereNotEstTermine() {
        this.listFiltered = this.getTasksWhereNotEstTermine;
    }
}


let mainList = new Liste();
let task1Example = new Tache("Faire des courses", 1, mainList);
let task2Example = new Tache("Faire du sport", 2, mainList);
mainList.ajouterTache(task1Example);
mainList.ajouterTache(task2Example);

const buttons = document.querySelectorAll('.btn-group .btn');

buttons.forEach(button => {
    button.addEventListener('click', () => actionForButton(buttons, button));
});

document.querySelector(".pb-4").addEventListener('submit', (event) => {
    event.preventDefault();
    let input = document.querySelector(".pb-4 input");
    let task = new Tache(input.value, mainList.taches.length + 1, mainList);
    mainList.ajouterTache(task);
    input.value = "";
    reloadListOnHTML();
});

reloadListOnHTML();

function reloadListOnHTML(){
    mainList.createdHtmlElement.innerHTML = "";
    mainList.listFiltered().forEach(element => {
        mainList.createdHtmlElement.appendChild(element.createdHtmlElement);
    });
}

function removeActive(buttons) {
    buttons.forEach(button => {
        button.classList.remove('active');
    });
}

function actionForButton(buttons, button) {
    removeActive(buttons);
    switch (button.getAttribute('data-filter')) {
        case 'all':
            mainList.setAllTasks();
            break;
        case 'done':
            mainList.setTasksWhereEstTermine();
            break;
        case 'todo':
            mainList.setTasksWhereNotEstTermine();
            break;
    }

    reloadListOnHTML();
    button.classList.add('active');
}